package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"strconv"
)

func main() {
	var gh, mp, as, r string
	flag.StringVar(&gh, "gh", "0", "Google Home Total")
	flag.StringVar(&mp, "mp", "0", "Macbook Pro Total")
	flag.StringVar(&as, "as", "0", "Alexa Speaker Total")
	flag.StringVar(&r, "r", "0", "Raspberry Pi B Total")
	flag.Parse()

	fmt.Println("Rules")
	fmt.Println("1. Each sale of a MacBook Pro comes with a free Raspberry Pi B\n2. Buy 3 Google Homes for the price of 2\n3. Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers")
	fmt.Println("")

	asTotal, alexaTotalPrice := getAlexaTotal(as)
	ghTotal, googleTotalPrice := getGoogleTotal(gh)
	mpTotal, rTotal, macTotalPrice, raspTotalPrice := getMacRasTotal(mp, r)

	fmt.Println("Macbook Pro:", mpTotal, "| Bonus Raspberry Pi B:", mpTotal, "| Price @1:", ProductMacMock.Price, "| Price Total:", macTotalPrice)
	fmt.Println("Google Home:", ghTotal, "| Price @1:", ProductGoogleMock.Price, "| Price Total:", googleTotalPrice)
	fmt.Println("Alexa Speaker:", asTotal, "| Price @1:", ProductAlexaMock.Price, " | Price Total:", alexaTotalPrice)
	fmt.Println("Raspberry Pi B:", rTotal, "| Price @1:", ProductRaspberryMock.Price, "| Price Total:", raspTotalPrice)
}

func getMacRasTotal(mp string, r string) (int, int, float64, float64) {
	mpTotal, err := strconv.Atoi(mp)
	if err != nil {
		log.Fatalln("Macbook Pro must be numeric")
	}

	rTotal, err := strconv.Atoi(r)
	if err != nil {
		log.Fatalln("Raspberry Pi B must be numeric")
	}

	var rGift int
	if mpTotal >= 1 {
		rGift = mpTotal
	}

	if rTotal < rGift {
		rTotal = 0
	} else if rTotal >= rGift {
		rTotal = rTotal - rGift
	}

	macTotal := float64(mpTotal) * ProductMacMock.Price
	raspTotal := float64(rTotal) * ProductRaspberryMock.Price
	return mpTotal, rTotal, macTotal, raspTotal
}

func getAlexaTotal(as string) (int, float64) {
	asTotal, err := strconv.Atoi(as)
	if err != nil {
		log.Fatalln("Alexa Speaker must be numeric")
	}
	var alexaTotal float64
	if asTotal >= 3 {
		alexaTotal = (float64(asTotal) * ProductAlexaMock.Price) - ((float64(asTotal) * ProductAlexaMock.Price) * 0.1)
	} else {
		alexaTotal = float64(asTotal) * ProductAlexaMock.Price
	}
	return asTotal, alexaTotal
}

func getGoogleTotal(gh string) (int, float64) {
	ghTotal, err := strconv.Atoi(gh)
	if err != nil {
		log.Fatalln("google home must be numeric")
	}
	var googleTotal float64
	googleMod := math.Mod(float64(ghTotal), 3)
	if googleMod == 0 {
		total := ghTotal / 3 * 2
		googleTotal = float64(total) * ProductGoogleMock.Price
	} else {
		total := ghTotal / 3 * 2
		rest := ghTotal % 3
		amount := total + rest
		googleTotal = float64(amount) * ProductGoogleMock.Price
	}
	return ghTotal, googleTotal
}

type Product struct {
	SKU   string  `json:"sku"`
	Name  string  `json:"name"`
	Price float64 `json:"price"`
	Stock uint64  `json:"stock"`
}

var ProductGoogleMock = Product{
	SKU:   "120P90",
	Name:  "Google Home",
	Price: 49.99,
	Stock: 10,
}

var ProductMacMock = Product{
	SKU:   "43N23P",
	Name:  "Macbook Pro",
	Price: 5399.99,
	Stock: 5,
}

var ProductAlexaMock = Product{
	SKU:   "A304SD",
	Name:  "Alexa Speaker",
	Price: 109.50,
	Stock: 10,
}

var ProductRaspberryMock = Product{
	SKU:   "234234",
	Name:  "Raspberry Pi B",
	Price: 30.00,
	Stock: 2,
}
