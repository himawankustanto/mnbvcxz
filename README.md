# mnbvcxz


## Run

```
$ go run main.go -gh 4 -mp 2 -as 4 -r 2
```

## Explanation
```
-gh is mean total google home that you want to buy
-mp is mean total macbook pro that you want to buy
-as is mean total alexa speaker that you want to buy
-r is mean total raspberry that you want to buy
```

## Rules
```
1. Each sale of a MacBook Pro comes with a free Raspberry Pi B
2. Buy 3 Google Homes for the price of 2
3. Buying more than 3 Alexa Speakers will have a 10% discount on all Alexa speakers
```

## GraphQL Adding Cart
```
  mutation{
    addCart(addCartRequest: {
      product_id: 123,
      quantity: 1,
      user_id: 123,
    }) {
      cart_id,
      product{
        product_name,
        product_free,
        quantity,
        quantity_product_free,
        price,
        total_price,
      }
    }
  }
```

## GraphQL Get Cart
```
query{
  getCart(getCartRequest: {user_id: 123}){
    cart_id,
    product{
        product_name,
        product_free,
        quantity,
        quantity_product_free,
        price,
        total_price,
    }
  }
}
```

## GraphQL Checkout
```
  mutation{
    checkout(checkoutRequest: {
      cart_id: [1, 2],
      user_id: 1234
    }) {
      order_id,
      user{
        first_name,
        last_name,
        email,
        phone_number,
      }
      quantity,
      total_amount,
      order_detail{
        product_name,
        product_free,
        quantity,
        quantity_product_free,
        price,
        total_price,
      }
    }
  }
```